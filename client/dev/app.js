;(function(ng) {
  'use strict';

  ng.module('node-mytest', [
      'ngResource',
      'ngRoute',
      'ngMessages'
    ]);
}(window.angular));
