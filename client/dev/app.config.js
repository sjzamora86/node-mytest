;(function(ng) {
  'use strict';

  ng.module('node-mytest')
    .config([
      '$locationProvider',
      function($locationProvider) {
        
        $locationProvider.html5Mode(true);
        
      }
    ]);
}(window.angular));
